#include "DRDictionaryReader.hpp"

#include <fstream>

std::vector<std::string> DRDictionaryReader::Read(const char* filePath)
{
	std::vector<std::string> result;
	std::ifstream inFile(filePath);
	std::string tempLine;

	if (inFile.is_open())
	{
		while (inFile >> tempLine)
		{
			result.emplace_back(tempLine);
		}
	}
	
	return result;
}
